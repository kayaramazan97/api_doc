
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should(); 
let path = 'http://10.0.23.231:8077/api/117/v1/docs/editDocument' //process.argv.pop()
let body = {}
process.argv.filter(item => item.includes(':')).forEach(item => { body[item.split(':')[0]]=item.split(':')[1]})
  
chai.use(chaiHttp);


describe('/POST query', () => {
    it('it should post to server', (done) => {
        let query = body
        chai.request(path)
            .post('/')
            .send(query)
            .end((err, res) => { 
                res.should.have.status(603); 
                done();
            });
    });

});