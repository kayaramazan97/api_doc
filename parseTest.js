
const request = require('request');
const fs = require('fs');
const prompts = require('prompts');
console.log('Test working')
const { exec } = require("child_process");


let config_json = []
const host_question = [
  {
    type: 'text',
    name: 'host',
    message: 'Host ( 127.0.0.1 ): '
  },
  {
    type: 'text',
    name: 'port',
    message: prev => prev.includes('https') ? 'Port ( 443 ) : ' : 'Port ( 80 ) : '
  }
];
const read_file = async (filepath) =>
  new Promise((res, rej) => {
    fs.readFile(filepath, function read(err, data) {
      if (err) {
        res([])
      }
      const content = data;
      res(JSON.parse(content.toString()))
    });
  })
const getFileContent = async () => {

  config_json = await read_file('./config_data.json')

  return

}
const getTest = async (url, obejct) =>
  new Promise(async (res, rej) => {
    exec(`npm run-script check getTest.js ${url}`, (err, stdout, stderr) => {
      console.log(stdout)
      res(stdout)
    });
  })

const postTest = async (url, obejct) =>
  new Promise(async (res, rej) => {
    let body = obejct.func.body_string.body
    console.log(body)
    exec(`npm run-script check postTest.js ${JSON.stringify(body)} ${url}`, (err, stdout, stderr) => {
      console.log(stdout)
      res(stdout)
    });
  })


const test = async () => {
  let question = await prompts(host_question);
  await getFileContent();
  let defPort = question.host.includes("https") ? '443' : '80'
  let port = question.port ? question.port : defPort
  let host = question.host ? question.host : '127.0.0.1'

  for (let i = 0; i < config_json.length; i++) {

    let actualPath = host + ':' + port + '/' + config_json[i].func.path + '?'
    for (const [key, value] of Object.entries(config_json[i].func.query_string))
      actualPath += key + '=' + value.val + '&&'
    actualPath = 'https://google.com:443'
    if (config_json[i].func.req_method == 'GET') {
      await getTest(actualPath, config_json[i])
    }
    else if(config_json[i].func.req_method == 'POST')
    {
     await postTest(actualPath, config_json[i])
    } 
  }
}
test()




