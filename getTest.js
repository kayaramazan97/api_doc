
let chai = require('chai');
let chaiHttp = require('chai-http'); 
let should = chai.should();   
let path = process.argv.pop() 
chai.use(chaiHttp);  
    describe('/GET index', () => {
        it('it should index with some message', (done) => {
            chai.request(path)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);  
                    done();
                });
        });
    });