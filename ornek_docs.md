* [func_name](#func_name)
* [func_name](#func_name)
* [funname](#funname)
## func_name


Service Descriptopn

**Auth Type** : Basic

Request parameters:

| Query String | Description | Is Require | Format |
| ------------ | ----------- | ---------- | ------ |
| func | func_name | M | 
cno | card no | M | 
bt | Boaarding Date Time | M | YYYYMMDDHHmmss |

```markdown
Request Method : POST
Request Body : {"func":"validate","cno":"0282000001800168","bt":"20201222120937"}
Request URL: http://{host}:{port}/HesServices/services?func=validate&&cno=0282000001800168&&bt=20201222120937
Response: true
```

## func_name


Service Descriptopn

**Auth Type** : -

Request parameters:

| Query String | Description | Is Require | Format |
| ------------ | ----------- | ---------- | ------ |
| func | func_name | M | 
cno | card no | M | 
bt | Boaarding Date Time | M | YYYYMMDDHHmmss |

```markdown
Request Method : GET
Request Body : -
Request URL: http://{host}:{port}/HesServices/services?func=validate&&cno=0282000001800168&&bt=20201222120937
Response: true
```

## funname


Service Descriptopn

**Auth Type** : -

Request parameters:

| Query String | Description | Is Require | Format |
| ------------ | ----------- | ---------- | ------ |
| func | funname | M | 
cno | card no | M | 
bt | Boaarding Date Time | M | YYYYMMDDHHmmss |

```markdown
Request Method : GET
Request Body : -
Request URL: http://{host}:{port}/HesServices/services?func=validate&&cno=0282000001800168&&bt=20201222120937
Response: true
```
